import { Field, ID, ObjectType } from "@nestjs/graphql";
import { type } from "os";
import { UserType } from "src/user/user.type";

@ObjectType('Note')
export class NoteType {
    @Field(type => ID)
    id: string;
    
    @Field()
    name:string;
    
    @Field()
    content: string;
    
    @Field()
    creationDate:string;

    @Field(type => UserType)
    creator: string;
}