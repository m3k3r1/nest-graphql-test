import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Note } from './note.entity';
import { NoteResolver } from './note.resolver';
import { NoteService } from './note.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([Note])
    ],
    providers: [NoteResolver, NoteService]
})
export class NoteModule {}
