import { Column, Entity, ObjectIdColumn, PrimaryColumn } from "typeorm";

@Entity()
export class Note {
    @ObjectIdColumn()
    _id:string

    @PrimaryColumn()
    id: string;
    
    @Column()
    name:string;
    
    @Column()
    content: string;
    
    @Column()
    creationDate:string;

    @Column()
    creator: string;
}