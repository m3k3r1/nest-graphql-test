import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Note } from './note.entity';
import { v4 as uuid } from 'uuid';
import { CreateNoteInput } from './note.input';
import { AssignUserInput } from './assignUser.input';

@Injectable()
export class NoteService {

    constructor(
        @InjectRepository(Note) private noteRepository: Repository<Note>
    ) {}


    async createNote(createNoteInput: CreateNoteInput): Promise<Note>{
       const { name, content } = createNoteInput;
       
        const note = this.noteRepository.create(
        {    
            id: uuid(),
            name,
            content,
            creationDate: (new Date()).toISOString(),
        });

        return this.noteRepository.save(note);
    }

    async getAllNotes(): Promise<Note[]> {
        return this.noteRepository.find();
    }

    async getNoteById(id): Promise<Note>{
        return this.noteRepository.findOne({id: id});
    }

    async asignUser(assignUserInput: AssignUserInput) : Promise<Note>{
        const { userId, noteId } = assignUserInput;

        const note = await this.noteRepository.findOne({id: noteId});
        note.creator = userId;

        return this.noteRepository.save(note);
    }
}