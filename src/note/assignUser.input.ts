import { Field, ID, InputType } from "@nestjs/graphql";
import { IsNotEmpty, IsUUID, MinLength } from "class-validator";

@InputType()
export class AssignUserInput {
    @IsUUID()
    @Field(type=> ID) 
    userId: string;
    
    @IsUUID()
    @Field(type=> ID) 
    noteId: string
}