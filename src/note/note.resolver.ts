import { Resolver, Query, Mutation, Args } from "@nestjs/graphql";
import { AssignUserInput } from "./assignUser.input";
import { CreateNoteInput } from "./note.input";
import { NoteService } from "./note.service";
import { NoteType } from "./note.type";

@Resolver(of => NoteType)
export class NoteResolver {

    constructor( 
        private noteService: NoteService
    ) {}

    @Query(returns => NoteType)
    note(
        @Args('id') id: string,
    ) {
        return this.noteService.getNoteById(id);
    }

    @Query(returns => [NoteType])
    notes() {
        return this.noteService.getAllNotes();
    }


    @Mutation(returns => NoteType) 
    createNote(
        @Args('createNoteInput') createNoteInput: CreateNoteInput 
    ){
        return this.noteService.createNote(createNoteInput);
    }

    @Mutation(returns => NoteType)
    assignUser(
        @Args('assignUserInput') assignUserInput: AssignUserInput
    ) {
        return this.noteService.asignUser(assignUserInput);
    }

}