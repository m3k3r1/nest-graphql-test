import { Field, InputType } from "@nestjs/graphql";
import { MinLength } from "class-validator";

@InputType()
export class CreateNoteInput {
    @MinLength(1)
    @Field() 
    name: string;
    
    @MinLength(1)
    @Field() 
    content: string
}