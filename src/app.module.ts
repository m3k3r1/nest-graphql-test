import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Note } from './note/note.entity';
import { NoteModule } from './note/note.module';
import { User } from './user/user.entity';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mongodb',
      url: 'mongodb://localhost/notes-test',
      useUnifiedTopology: true,
      entities: [
        Note,
        User
      ]
    }),
    GraphQLModule.forRoot({
      autoSchemaFile: true
    }),
    NoteModule,
    UserModule
  ]
})
export class AppModule {}
