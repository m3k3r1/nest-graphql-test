import { Resolver, Query, Mutation, Args } from "@nestjs/graphql";
import { CreateUserInput } from "./createUser.input";
import { UserService } from "./user.service";
import { UserType } from "./user.type";


@Resolver(of => UserType)
export class UserResolver {

    constructor( 
        private userService: UserService
    ) {}

    @Query(returns => UserType)
    user(
        @Args('id') id: string,
    ) {
        return this.userService.getUserById(id);
    }

    @Query(returns => [UserType])
    users() {
        return this.userService.getAllUsers();
    }


    @Mutation(returns => UserType) 
    createUser(
        @Args('createUserInput') createUserInput: CreateUserInput 
    ){
        return this.userService.createUser(createUserInput);
    }
}