import { IsNotEmpty, MinLength } from "class-validator";
import { Field, InputType } from "@nestjs/graphql";

@InputType()
export class CreateUserInput {
    @Field()
    @IsNotEmpty()
    firstName:string;
    
    @Field()
    @IsNotEmpty()

    lastName: string;
    
    @Field()
    @IsNotEmpty()
    username:string;
}