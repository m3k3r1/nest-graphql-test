import { Field, ID, ObjectType } from "@nestjs/graphql";
import { type } from "os";

@ObjectType('User')
export class UserType {
    @Field(type => ID)
    id: string;
    
    @Field()
    firstName:string;
    
    @Field()
    lastName: string;
    
    @Field()
    username:string;
}