import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserInput } from './createUser.input';
import { User } from './user.entity';
import { v4 as uuid } from 'uuid';


@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User) private userRepository: Repository<User>
    ) {}

    createUser(createUserInput: CreateUserInput): Promise<User>{
        const { firstName, lastName,  username} = createUserInput;
        
         const note = this.userRepository.create(
         {    
             id: uuid(),
             firstName, 
             lastName,  
             username
         });
 
         return this.userRepository.save(note);
     }
 
     getAllUsers(): Promise<User[]> {
         return this.userRepository.find();
     }
 
     getUserById(id): Promise<User>{
         return this.userRepository.findOne({id: id});
     }
}
